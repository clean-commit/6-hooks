# 6 git hooks

Run `mv .git/hooks/pre-commit.sample .git/hooks/pre-commit` for git-hook test

and then edit on first line like below:

```
vi .git/hooks/pre-commit.sample

echo "after activation pre-commit sample, then you can receive warning about trailing spaces"

# ... blablabla
```

Default git pre-commit would not allow commit with white spaces       


See package list with result by tree command

```
node_modules/
└── husky
    ├── LICENSE
    ├── README.md
    ├── husky.sh
    ├── lib
    │   ├── bin.d.ts
    │   ├── bin.js
    │   ├── index.d.ts
    │   └── index.js
    └── package.json

2 directories, 8 files
```
